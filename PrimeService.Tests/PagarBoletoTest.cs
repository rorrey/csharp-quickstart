﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace PrimeService.Tests
{
    class PagarBoletoTest
    {
        [Test]
        public void ReturnFIsNullorEmpty()
        {
            STM stm = new STM();
            Tarjeta tar = stm.CargarTarjeta(1,100);

            stm.PagarBoletoTest(tar,25,185,DateTime.Now);

            Assert.IsTrue(tar.Saldo == 75);
        }
    }
}
