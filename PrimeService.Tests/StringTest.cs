﻿using System;
using System.Collections.Generic;
using System.Text;
using NUnit.Framework;

namespace PrimeService.Tests
{
    class StringTest
    {

        private readonly string _primeService;

    
        [Test]
        public void ReturnFIsNullorEmpty()
        {
            var result = string.IsNullOrEmpty("");

            Assert.IsTrue(result, "\"\" should be empty");
        }


        [Test]
        public void CantcharactertestVAVA()
        {
            StringCount c = new StringCount();
            Dictionary<string, int> result = c.Cantcharacter("vava");
            Dictionary<string, int> dic = new Dictionary<string, int>
            {
                { "v", 2 },
                { "a", 2 }
            };
            Assert.IsTrue(dic["v"] == result["v"] && 2 == result["a"]);

        }

        [Test]
        public void CantcharactertestPAPA()
        {
            StringCount c = new StringCount();
            Dictionary<string, int> result = c.Cantcharacter("papa");
            Assert.IsTrue(2 == result["p"] && 2 == result["a"]);

        }

    }
}
