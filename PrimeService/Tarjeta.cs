﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrimeService
{
    public class Tarjeta
    {
        public int Id { get; set; }
        public int Saldo { get; set; }

        public void DescontarSaldo(int costo)
        {
            Saldo = Saldo - costo;
        }
    }
}
