﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrimeService
{
    public class STM
    {
        public Tarjeta CargarTarjeta(int id, int saldo)
        {

            Tarjeta t = new Tarjeta
            {
                Id = id,
                Saldo = saldo
            };
            return t;

        }

        public void PagarBoletoTest(Tarjeta tar, int costo, int bus, DateTime fecha)
        {
            tar.DescontarSaldo(costo);
        }
    }
}
