﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PrimeService
{
    public class StringCount
    {
        public Dictionary<string, int> Cantcharacter(string stanalizar)
        {
            Dictionary<string, int> res = new Dictionary<string, int>();
            char[] a = stanalizar.ToCharArray();
            foreach (var c in a)
            {
                if (res.ContainsKey(c.ToString()))
                {
                    res[c.ToString()] = res[c.ToString()] + 1;

                }
                else
                {
                    res.Add(c.ToString(), 1);
                }
            }
            //Dictionary<string, int> res = new Dictionary<string, int>
            //{
            //    { "m", 2 },
            //    { "a", 2 }
            //};

            return res;
            
        }
    }
}
